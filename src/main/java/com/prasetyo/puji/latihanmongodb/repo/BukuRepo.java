package com.prasetyo.puji.latihanmongodb.repo;

import com.prasetyo.puji.latihanmongodb.entity.Buku;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/* bawaan
public interface BukuRepo extends Repository<Buku, Long> {
*/

@Repository
public interface BukuRepo extends MongoRepository<Buku, String> {
    Buku findBy_id(ObjectId _id);
    // Page<Buku> findAll(Pageable pageable);

    // Buku findByNameAndStateAllIgnoringCase(String name);

}