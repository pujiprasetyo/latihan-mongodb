package com.prasetyo.puji.latihanmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LatihanMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(LatihanMongodbApplication.class, args);
	}

	// @Bean
	// public AlwaysSampler defaultSampler() {
	// 	return new AlwaysSampler();
	// }
}