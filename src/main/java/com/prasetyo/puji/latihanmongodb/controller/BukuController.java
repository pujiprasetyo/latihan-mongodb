package com.prasetyo.puji.latihanmongodb.controller;

//import java.util.List;
import javax.validation.Valid;
import com.prasetyo.puji.latihanmongodb.entity.Buku;
import com.prasetyo.puji.latihanmongodb.repo.BukuRepo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/buku")
public class BukuController {
    @Autowired
    private BukuRepo bukuRepo;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Iterable<Buku>> getAll() {
        try {
            return new ResponseEntity<Iterable<Buku>>(this.bukuRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // public List<Buku> getAllPets() {
    // return bukuRepo.findAll();
    // }

    // @RequestMapping(method = RequestMethod.GET, value = "/biodata", produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // ResponseEntity<Iterable<Biodata>> getAll() {
    // try {
    // return new ResponseEntity<Iterable<Biodata>>(this.biodataRepo.findAll(),
    // HttpStatus.OK);
    // } catch (Exception e) {
    // return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    // }
    // }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Buku getPetById(@PathVariable("id") ObjectId id) {
        return bukuRepo.findBy_id(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void modifyPetById(@PathVariable("id") ObjectId id, @Valid @RequestBody Buku buku) {
        buku.setId(id);
        bukuRepo.save(buku);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Buku createPet(@Valid @RequestBody Buku buku) {
        buku.setId(ObjectId.get());
        bukuRepo.save(buku);
        return buku;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePet(@PathVariable ObjectId id) {
        bukuRepo.delete(bukuRepo.findBy_id(id));
    }
}